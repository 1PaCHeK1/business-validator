# Business-Validator

## ErrorSchema
Use business_validator.ErrorSchema if you don't know which format you're gonna use.

## Quick Start

### DTO
```python
class CommentDto(BaseModel):
    comment: str
    post_id: int
    owner_id: int
```


### Add error source (error context)
```python
class Source(BaseModel):
    local: str
```

### DTO Validator
```python
@dataclasses.dataclass()
class CommentValidator(Validator[ErrorSchema[Source]]):
    dto: CommentDto

    @validate
    async def test1(self):
        post_ids = list(range(1, 10))

        if self.dto.post_id not in post_ids:
            self.context.add_error(
                ErrorSchema(
                    code=ErrorCodeEnum.not_found.value,
                    message="Id doen't not exists",
                    detail=f"Post with id={self.dto.post_id} not found",
                    source=Source(
                        local="data/post_id",
                    ),
                )
            )

    @validate
    async def test2(self):
        owner_ids = list(range(1, 10))

        if self.dto.owner_id not in owner_ids:
            self.context.add_error(
                ErrorSchema(
                    code=ErrorCodeEnum.not_found.value,
                    message="Id doen't not exists",
                    detail=f"User with id={self.dto.post_id} not found",
                    source=Source(
                        local="data/owner_id",
                    ),
                )
            )
```

### Use
```python
async def function():
    validator = CommentValidator(...)
    errors = await validator.errors()
    # or
    await validator.validate() # raise ValidationError

```
